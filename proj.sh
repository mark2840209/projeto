#!/bin/bash

password=$(yad --title "Digite a senha root para continuar" --entry)

function menu() {
	yad --form --center --width=300 --title="Menu Principal" \
		--button="Verificar Servidor SSH":0 \
		--button="Instalar Servidor SSH":1 \
		--button="Iniciar Transferência de Arquivos":2 \
		--buttons-layout=center \
		--on-top \
		--text="Interface de Transferência de Arquivos SCP" \
		--windw-icon="dialog-question" \
		--image="dialog.question" \
		--image-on-top
	option=$?
	case $option in
		0) verify_ssh ;;
		1) install_ssh ;;
		2) save_ip ;;
		*) yad --text "Opção Inválida" ;; 
	esac
}

function save_ip() {
	user_ips=$(cat server_ips.txt)
	server_ip=$(yad --title "Iniciando conexão SSH" --text "Digite o USER@IP do servidor" --entry $user_ips --editable)
	if [[ -n $server_ip ]]; then
		if [[ -e server_ips.txt ]]; then
			mv server_ips.txt server_ips_tmp.txt
			echo "$server_ip" > server_ips.txt
			cat server_ips_tmp.txt >> server_ips.txt
			rm server_ips_tmp.txt
		else
			echo "$server_ip" > server_ips.txt
		fi
	fi
	submenu
}

function install_ssh() {
	echo "$password" | sudo -S apt-get install openssh-server && yad --text "Instalação concluída com sucesso!" || yad --text "Falha na instalação"
	menu
}

function verify_ssh() {
	if dpkg -l | grep -q openssh-server; then
		yad --text "O servidor SSH já está instalado"
	else	
		yad --text "O servidor SSH não está instalado, por favor, instale-o" \
		--button="Instalar SSH":0 \
		--button="Fechar":1
		option=$?
		case $option in
			0) install_ssh ;;
			1) exit 0 ;;
		esac
	fi
	menu
}

function list_files() {
	ls > client_files.txt
	client_files=$(cat client_files.txt)
	server_ip=$(head -n 1 server_ips.txt)
	sshpass -p $password ssh $server_ip 'ls > server_files.txt'
	sshpass -p $password scp $server_ip:/home/ifpb/server_files.txt /home/ifpb/project/script/
	server_files=$(cat server_files.txt)
	array=("${server_files[@]}" "${client_files[@]}")
	yad --width=900 --height=900 --list --title "Arquivos" --column "Arquivos do Servidor" --column "Arquivos do Cliente" "${array[@]}"
	if [[ $1 -eq 2 ]]; then
		submenu
	fi
}

function verify_copy() {
	server_ip=$(head -n 1 server_ips.txt)
	source_file=$2
	destination=$3
	md5_source=$(md5sum "$source_file" | awk '{print $1}')
	md5_copy=$(sshpass -p ifpb ssh "$server_ip" "cd $destination && md5sum $source_file" | awk '{print $1}')
	if [[ "$md5_source" == "$md5_copy" ]]; then
		yad --title "Verificação por md5" --text "Cópia bem-sucedida!"
	else
		yad --title "Verificação por md5" --text "Cópia mal-sucedida!"
	fi
}

function scp_s_to_c() {
	list_files 1
	file=$(yad --title "Qual arquivo deseja copiar?" --entry)
	server_ip=$(head -n 1 server_ips.txt)
	server_path=$(yad --title "Digite o caminho do arquivo no servidor" --entry)
	client_path=$(yad --title "Digite o caminho de destino no cliente" --entry)
	sshpass -p $password scp $server_ip:$server_path$file $client_path && verify_copy 1 $file $server_path
}

function scp_c_to_s() {
	list_files 1
	file=$(yad --title "Qual arquivo deseja copiar?" --entry)
	server_ip=$(head -n 1 server_ips.txt)
	server_path=$(yad --title "Digite o caminho de destino no servidor" --entry)
	sshpass -p $password scp $file $server_ip:$server_path && verify_copy 2 $file $server_path
}

function submenu() {
	yad --form --center --width=300 --title="Menu de Cópia" \
		--button="Copiar do Servidor para o Cliente":0 \
		--button="Copiar do Cliente para o Servidor":1 \
		--button="Verificar Lista de Arquivos":2 \
		--buttons-layout=center --on-top \
		--text="Selecione a operação de cópia desejada"
	option=$?
	case $option in
		0) scp_s_to_c  ;;
		1) scp_c_to_s  ;;
		2) list_files 2 ;;
		*) yad --text "Opção Inválida" ;;
	esac
}

menu


